#include <emmintrin.h>
#include <x86intrin.h>
#include <stdio.h>
#include <stdint.h>

u_int8_t array[10*4096];
int main(int argc, const char **argv) {
	uint junk=0;
	register uint64_t time1, time2;
	volatile uint8_t *addr,a;
	int i;
       	uint8_t ele1,ele2;
	int meanNoFlush = 0;
	int meanNoFlushNb = 0;
	int meanFlush = 0;
	int meanFlushNb = 0;


	for(int j=0;j<10;j++){
		// Initialize the array
		for(i=0;i<10;i++){
			array[i*4096] = 1;
		}
		// FLUSH the array from the CPU cache
		for(i=0;i<10;i++){
			_mm_clflush(&array[i*4096]);
		}

		// Access some of the array items
		printf("Read 3 : %d\n",array[3*4096]);
		ele1 = array[3*4096];
		printf("Read 7 : %d\n",array[7*4096]);
		ele2 = array[7*4096];


		// default Time

		//measure access time of all items 
		for(i=0; i<10; i++) {
			time1=__rdtscp(&junk);
			a = array[i*4096];
			time2=__rdtscp(&junk);

			//printf("Access time for array[%d*4096]: %d CPU cycles\n",i, (int)time2);
			printf("Access time for array[%d*4096]: %d CPU cycles\n",i, (int)time2 - (int)time1);
			if(i == 3 || i == 7){
				meanNoFlush += (int)time2 - (int)time1;		
				meanNoFlushNb ++;
			}else{
				meanFlush += (int)time2 - (int)time1;		
				meanFlushNb ++;
			}
		}

	}
	printf("Mean No Flush : %d\n",meanNoFlush/meanNoFlushNb);
	printf("Mean Flush : %d\n",meanFlush/meanFlushNb);
	return 0; 
}
