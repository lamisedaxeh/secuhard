KVERS = $(shell uname -r)

# Kernel modules
obj-m += MeltdownKernel.o

build: kernel_modules

kernel_modules:
	make -C /usr/lib/modules/$(KVERS) M=$(CURDIR) modules

clean:
	make -C /usr/lib/modules/$(KVERS) M=$(CURDIR) clean
